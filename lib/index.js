/*!
 * Copyright (C) 2017 Ole Reglitzki
 *
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General
 * Public License as published by the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License for more
 * details.
 *
 * You should have received a copy of the GNU Affero General Public License along with this program. If not, see
 * <http://www.gnu.org/licenses/>.
 */
"use strict";

const Plugin = require("./Plugin");

/*===================================================== Exports  =====================================================*/

module.exports = use;

/*==================================================== Functions  ====================================================*/

function use(renato, config) {
  let plugin = new Plugin(renato, config);
  renato.addHandler("core::routes:bind", plugin.bindRoutes.bind(plugin), -1);
  return plugin;
}
