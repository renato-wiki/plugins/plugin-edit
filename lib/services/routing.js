/*!
 * Copyright (C) 2017 Ole Reglitzki
 *
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General
 * Public License as published by the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License for more
 * details.
 *
 * You should have received a copy of the GNU Affero General Public License along with this program. If not, see
 * <http://www.gnu.org/licenses/>.
 */
"use strict";

const _ = require("lodash");
const Promise = require("bluebird");
const parseURI = require("@renato-wiki/core/lib/services/parse-uri");

/*===================================================== Exports  =====================================================*/

exports.bind = bindRoutes;

/*==================================================== Functions  ====================================================*/

function bindRoutes(renato, router) {
  let createFn = _.partial(createContent, renato);
  router.post("/content", createFn);
  router.post("/content/*", createFn);
  let upsertFn = _.partial(upsertContent, renato);
  router.put("/content", upsertFn);
  router.put("/content/*", upsertFn);
}

function createContent(renato, req, res, next) {
  let uri = parseURI(req.params[0]);
  renato.apiData
      .getContentData(uri)
      .then((contentData) => {
        if (contentData.index != null) { return Promise.reject({status: 400, message: "Path already in use."}); }
        return create(renato, uri, req.body);
      })
      .then(_.ary(next, 0), next);
}

function upsertContent(renato, req, res, next) {
  let uri = parseURI(req.params[0]);
  renato.apiData
      .getContentData(uri)
      .then((contentData) => {
        if (contentData.index == null) { return create(renato, uri, req.body); }
        if (contentData.index.parent == null) {
          return Promise.reject({status: 400, message: "Root path not allowed."});
        }
        return update(renato, contentData, req.body);
      })
      .then(_.ary(next, 0), next);
}

function create(renato, uri, body) {
  body.id = uri.pop();
  if (body.meta === void 0) {
    body.meta = {title: body.id};
  } else if (typeof body.meta.title !== "string") {
    body.meta.title = body.id;
  }
  return renato.apiData
      .getContentData(uri)
      .then((parentContentData) => {
        body.parentContentData = parentContentData;
        if (parentContentData.index == null || parentContentData.index.type !== "category") {
          return Promise.reject({status: 400, message: "Parent category not found."});
        }
        switch (body.type) {
          case "category":
            return renato
                .hookReduce("category::upsert:request", body)
                .then(_.partial(createCategory, renato));
          case "page":
            return renato
                .hookReduce("page::upsert:request", body)
                .then(_.partial(createPage, renato));
          default:
            return Promise.reject({status: 400, message: "Index type '" + body.type + "' not supported."});
        }
      });
}

function update(renato, contentData, body) {
  body.contentData = contentData;
  switch (contentData.index.type) {
    case "category":
      return renato
          .hookReduce("category::upsert:request", body)
          .then((data) => renato.hookReduce("category::update", data));
    case "page":
      return renato
          .hookReduce("page::upsert:request", body)
          .then((data) => renato.hookReduce("page::update", data));
    default:
      throw new Error("Index type '" + contentData.index.type + "' not supported.");
  }
}

function createCategory(renato, data) {
  if (data.meta == null) { return Promise.reject({status: 400, message: "No meta provided."}); }
  return renato.hookReduce("category::create", data);
}

function createPage(renato, data) {
  if (data.content == null) { return Promise.reject({status: 400, message: "No content provided."}); }
  if (typeof data.content !== "string" && !(data.content instanceof Buffer)) {
    throw new Error("Page content has not been serialized.");
  }
  return renato.hookReduce("page::create", data);
}
